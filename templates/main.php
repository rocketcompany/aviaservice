<?php /* Template Name: Главная */ ?>
<?php get_header() ?>

<?php
$hero = get_field('hero');
$bgi_tablet = wp_get_attachment_image_url($hero['background_image_tablet'], 'full');
$bgi_mobile = wp_get_attachment_image_url($hero['background_image_mobile'], 'full');

$services = get_field('services');
$shipping_stages = get_field('shipping_stages');
$shipping_stages_image = wp_get_attachment_image_url($shipping_stages['image'], 'full');
$shipping_stages_image_tablet = wp_get_attachment_image_url($shipping_stages['image_tablet'], 'full');

$trust = get_field('trust');
?>

	<main class="main">
		<div class="hero" style="--bgi-tablet: url(<?= $bgi_tablet ?>); --bgi-mobile: url(<?= $bgi_mobile ?>);">
			<div class="hero__grid">
				<div class="hero__left"></div>

				<div class="hero__right"></div>
			</div>

			<div class="container hero__container">
				<div class="hero__content">
					<h1 class="hero__title h1">
						<?= $hero['title'] ?>
					</h1>

					<h2 class="hero__subtitle h2">
						<?= $hero['subtitle'] ?>
					</h2>

					<div class="hero__text">
						<?= $hero['text'] ?>
					</div>

					<div class="hero__cta">
						<button type="button" class="button button--primary hero__button hero__button-first"
								data-bs-toggle="modal"
								data-bs-target="#modalOrderTransportation"
						>
							<?= __('Заказать', 'air') ?>
						</button>

						<a href="<?= get_the_permalink($hero['link']) ?>"
						   class="button button--secondary hero__button hero__button-second"
						>
							<?= __('Детальнее', 'air') ?>
							<span class="button__icon button__icon--right">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
							</svg>
						</span>
						</a>
					</div>
				</div>
			</div>
		</div>

		<section class="home-services">
			<div class="container home-services__container">
				<div class="home-services__grid">
					<div class="home-services__left">
						<h2 class="home-services__title h2">
							<?= $services['title'] ?>
						</h2>
					</div>

					<div class="home-services__right">
						<div class="home-services__text">
							<?= $services['text'] ?>
						</div>
					</div>
				</div>

				<div class="home-services__carousel">
					<div class="home-services__slider swiper">
						<div class="swiper-wrapper">
							<?php foreach ($services['list'] as $service): ?>
								<div class="swiper-slide">
									<div class="home-services-slide">
										<div class="home-services-slide__image">
											<?= wp_get_attachment_image(get_field('image', $service->ID), 'full', false, ['loading' => false]) ?>
										</div>

										<div class="home-services-slide__content">
											<h3 class="home-services-slide__title h3">
												<?= $service->post_title ?>
											</h3>

											<div class="home-services-slide__text">
												<?= get_field('description', $service->ID) ?>
											</div>

											<a href="<?= get_the_permalink($service->ID) ?>"
											   class="home-services-slide__link button button--secondary"
											>
												<?= __('Детальнее', 'air') ?>
												<span class="button__icon button__icon--right">
											<svg>
												<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
											</svg>
										</span>
											</a>
										</div>
									</div>
								</div>
							<?php endforeach ?>
						</div>
					</div>

					<div class="home-services__navigation">
						<button class="button-slider-navigation home-services__navigation-prev">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-left"></use>
							</svg>
						</button>
						<button class="button-slider-navigation home-services__navigation-next">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
							</svg>
						</button>
					</div>

					<div class="as-slider__pagination home-services__pagination">
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</section>

		<section class="shipping-stages">
			<div class="container shipping-stages__container">
				<div class="shipping-stages__grid">
					<div class="shipping-stages__column">
						<h2 class="shipping-stages__title h2">
							<?= $shipping_stages['title'] ?>
						</h2>
					</div>

					<div class="shipping-stages__column">
						<div class="shipping-stages__text">
							<?= $shipping_stages['text'] ?>
						</div>
					</div>

					<div class="shipping-stages__column">
						<div class="shipping-stages__navigation shipping-stages-navigation">
							<ul class="shipping-stages-navigation__list">
								<?php foreach ($shipping_stages['stages'] as $key => $item): ?>
									<li class="shipping-stages-navigation__item">
										<div class="shipping-stages-navigation__stage">
											<?= sprintf("%d %s", $key + 1, __('этап', 'air')) ?>
										</div>

										<h4 class="shipping-stages-navigation__title h4">
											<?= $item['title'] ?>
										</h4>
									</li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>

					<div class="shipping-stages__column">
						<div class="shipping-stages__image">
							<picture>
								<source media="(max-width: 1024px)"
										srcset="<?= $shipping_stages_image_tablet ?>"
								>
								<source media="(min-width: 1025px)"
										srcset="<?= $shipping_stages_image ?>"
								>
								<img src="<?= $shipping_stages_image ?>" alt="">
							</picture>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/sections/transportation-possibility') ?>

		<section class="trust">
			<div class="container trust__container">
				<h2 class="trust__title h2">
					<?= __('Нам доверяют', 'air') ?>
				</h2>

				<div class="trust__grid">
					<?php foreach ($trust['list'] as $item): ?>
						<div class="trust__item">
							<div class="trust__item-logo">
								<?= wp_get_attachment_image($item['logo'], 'medium_large') ?>
							</div>
						</div>
					<?php endforeach ?>
				</div>

				<div class="trust__carousel">
					<div class="trust__slider swiper">
						<div class="swiper-wrapper">
							<?php foreach ($trust['list'] as $item): ?>
								<div class="swiper-slide">
									<div class="trust__slide">
										<div class="trust__slide-logo">
											<?= wp_get_attachment_image($item['logo'], 'medium_large', false, ['loading' => false]) ?>
										</div>
									</div>
								</div>
							<?php endforeach ?>
						</div>
					</div>

					<div class="trust__navigation">
						<button class="button-slider-navigation trust__navigation-prev">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-left"></use>
							</svg>
						</button>

						<button class="button-slider-navigation trust__navigation-next">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
							</svg>
						</button>
					</div>

					<div class="as-slider__pagination trust__pagination">
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/sections/order-cargo') ?>
	</main>

<?php get_footer() ?>