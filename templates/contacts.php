<?php /* Template Name: Контакты */ ?>
<?php get_header() ?>

<?php
$title = get_the_title();
$contacts = get_field('contacts', 'option');
$hero = get_field('hero');
$bgi = wp_get_attachment_image_url($hero['background_image'], 'full');

$integrations = get_field('integrations', 'option');

$instagram = get_field('instagram');
?>

	<main class="main">
		<div class="contacts-page-hero" style="--bgi: url(<?= $bgi ?>)">
			<div class="contacts-page-hero__grid">
				<div class="contacts-page-hero__left"></div>

				<div class="contacts-page-hero__right"></div>
			</div>

			<div class="container contacts-page-hero__container">
				<div class="breadcrumbs breadcrumbs--contacts">
					<ul class="breadcrumbs__list">
						<li class="breadcrumbs__item">
							<a href="<?= home_url() ?>" class="breadcrumbs__link">
								<?= __('Главная', 'air') ?>
							</a>
						</li>
						<li class="breadcrumbs__item">
							<div class="breadcrumbs__current">
								<?= $title ?>
							</div>
						</li>
					</ul>
				</div>

				<div class="contacts-page-hero__content">
					<h1 class="contacts-page-hero__title h1">
						<?= $hero['title'] ?>
					</h1>

					<div class="contacts-page-hero__subtitle">
						<?= $hero['subtitle'] ?>
					</div>

					<div class="contacts-page-hero__contacts contacts-page-hero-contacts">
						<ul class="contacts-page-hero-contacts__list">
							<li class="contacts-page-hero-contacts__item">
								<a href="tel:<?= sanitize_phone($contacts['phone']) ?>"
								   class="contacts-page-hero-contacts__link"
								>
									<span class="contacts-page-hero-contacts__item-icon">
										<svg>
											<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
										</svg>
									</span>

									<span class="contacts-page-hero-contacts__link-text">
										<?= $contacts['phone'] ?>
									</span>
								</a>
							</li>

							<li class="contacts-page-hero-contacts__item">
								<a href="mailto:<?= $contacts['email'] ?>" class="contacts-page-hero-contacts__link">
									<span class="contacts-page-hero-contacts__item-icon">
										<svg>
											<use xlink:href="<?= get_sprite_uri() ?>#email"></use>
										</svg>
									</span>

									<span class="contacts-page-hero-contacts__link-text">
										<?= $contacts['email'] ?>
									</span>
								</a>
							</li>

							<?php if ($integrations['instagram']['enabled']): ?>
								<li class="contacts-page-hero-contacts__item">
									<a href="<?= $contacts['social']['instagram']['url'] ?>"
									   class="contacts-page-hero-contacts__link" target="_blank"
									>
									<span class="contacts-page-hero-contacts__item-icon">
										<svg>
											<use xlink:href="<?= get_sprite_uri() ?>#instagram"></use>
										</svg>
									</span>

										<span class="contacts-page-hero-contacts__link-text">
										<?= $contacts['social']['instagram']['title'] ?>
									</span>
									</a>
								</li>
							<?php endif ?>

							<li class="contacts-page-hero-contacts__item">
								<div class="contacts-page-hero-contacts__text">
									<span class="contacts-page-hero-contacts__item-icon">
										<svg>
											<use xlink:href="<?= get_sprite_uri() ?>#map-marker"></use>
										</svg>
									</span>

									<?= $contacts['address'] ?>
								</div>
							</li>

							<li class="contacts-page-hero-contacts__item">
								<div class="contacts-page-hero-contacts__text">
									<span class="contacts-page-hero-contacts__item-icon">
										<svg>
											<use xlink:href="<?= get_sprite_uri() ?>#mailbox"></use>
										</svg>
									</span>

									<?= sprintf("%s: %s", __('Почтовый адрес', 'air'), $contacts['mailing_address']) ?>
								</div>
							</li>

							<?php if (apply_filters('wpml_current_language', null) === 'ru'): ?>
								<li class="contacts-page-hero-contacts__item">
									<div class="contacts-page-hero-contacts__text-note">
										<?= $contacts['note'] ?>
									</div>
								</li>
							<?php endif ?>
						</ul>
					</div>

					<div class="contacts-page-hero__cta">
						<button type="button" class="button button--primary contacts-page-hero__button"
								data-bs-toggle="modal"
								data-bs-target="#modalCallback"
						>
							<span class="button__icon button__icon--left">
								<svg>
									<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
								</svg>
							</span>
							<?= __('Заказать звонок', 'air') ?>
						</button>
					</div>
				</div>

				<div class="contacts-page-hero__map">
					<div id="map"
						 data-settings="<?= htmlspecialchars($contacts['map'], ENT_QUOTES) ?>"
						 data-api-key="<?= $integrations['yandex_map']['api_key'] ?>"
					></div>
				</div>
			</div>
		</div>

		<?php if ($integrations['instagram']['enabled']): ?>
			<div class="instagram-feed">
				<div class="container instagram-feed__container">
					<div class="instagram-feed__grid">
						<div class="instagram-feed__column">
							<h2 class="instagram-feed__title h2">
								<?= __('Мы в instagram', 'air') ?>
							</h2>
						</div>

						<div class="instagram-feed__column">
							<div class="instagram-feed__text">
								<?= $instagram['description'] ?>
							</div>

							<a href="<?= $contacts['social']['instagram']['url'] ?>" target="_blank"
							   class="button button--secondary instagram-feed__link"
							>
							<span class="button__icon button__icon--left instagram-feed__link-icon1">
								<svg>
									<use xlink:href="<?= get_sprite_uri() ?>#instagram1"></use>
								</svg>
							</span>
								<?= __('Смотреть больше', 'air') ?>
								<span class="button__icon button__icon--right instagram-feed__link-icon2">
								<svg>
									<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
								</svg>
							</span>
							</a>
						</div>
					</div>
				</div>

				<?= do_shortcode('[instagram-feed feed=1]') ?>
			</div>
		<?php endif ?>
	</main>

<?php get_footer() ?>