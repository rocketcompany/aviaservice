<?php /* Template Name: О нас */ ?>
<?php get_header() ?>

<?php
$hero = get_field('hero');
$title = get_the_title();

$bgi_tablet = wp_get_attachment_image_url($hero['background_image_tablet'], 'full');
$bgi_mobile = wp_get_attachment_image_url($hero['background_image_mobile'], 'full');
?>

	<main class="main">
		<div class="about-hero" style="--bgi-tablet: url(<?= $bgi_tablet ?>); --bgi-mobile: url(<?= $bgi_mobile ?>);">
			<div class="about-hero__grid">
				<div class="about-hero__left"></div>

				<div class="about-hero__right"></div>
			</div>

			<div class="container about-hero__container">
				<div class="breadcrumbs breadcrumbs--about-page">
					<ul class="breadcrumbs__list">
						<li class="breadcrumbs__item">
							<a href="<?= home_url() ?>" class="breadcrumbs__link">
								<?= __('Главная', 'air') ?>
							</a>
						</li>
						<li class="breadcrumbs__item">
							<div class="breadcrumbs__current">
								<?= $title ?>
							</div>
						</li>
					</ul>
				</div>

				<div class="about-hero__content">
					<h1 class="about-hero__title h1">
						<?= $hero['title'] ?>
					</h1>

					<h2 class="about-hero__subtitle h1">
						<?= $hero['subtitle'] ?>
					</h2>

					<div class="about-hero__image">
						<?= wp_get_attachment_image($hero['background_image_mobile'], 'large') ?>
					</div>

					<div class="about-hero__text">
						<?= $hero['text'] ?>
					</div>
				</div>
			</div>
		</div>

		<?php get_template_part('template-parts/sections/order-cargo', null, ['classes' => 'order-cargo--about-page']) ?>
	</main>

<?php get_footer() ?>