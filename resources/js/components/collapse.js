import Collapse from 'bootstrap/js/dist/collapse'

const $companyFeaturesReadMore = document.querySelector('.company-features__read-more')
$companyFeaturesReadMore?.addEventListener('click', e => {
	const $companyFeaturesList = document.querySelector('.company-features__list')
	$companyFeaturesList.style.height = `${ $companyFeaturesList.scrollHeight }px`

	setTimeout(() => {
		$companyFeaturesList.style.height = `auto`

		e.target.remove()
	}, 300)
})

const $footerColumnTitleCollapse = document.querySelectorAll('.footer__column-title--collapse')
$footerColumnTitleCollapse.forEach(el => {
	el.addEventListener('click', e => {
		if (!window.matchMedia("(max-width: 767px)").matches) return

		const $collapse = e.target.closest('.footer__column').querySelector('.footer__collapse')
		const collapse = Collapse.getOrCreateInstance($collapse)
		collapse.toggle()
	})
})

const $footerCollapse = document.querySelectorAll('.footer__collapse')
$footerCollapse.forEach(el => {
	el.addEventListener('show.bs.collapse', e => {
		const $title = e.target.closest('.footer__column').querySelector('.footer__column-title--collapse')
		$title.classList.add('_show')
	})

	el.addEventListener('hide.bs.collapse', e => {
		const $title = e.target.closest('.footer__column').querySelector('.footer__column-title--collapse')
		$title.classList.remove('_show')
	})
})