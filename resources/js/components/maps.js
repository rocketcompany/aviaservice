import ymaps from 'ymaps'

const $map = document.getElementById('map')

if ($map && 'IntersectionObserver' in window) {
	const mapIO = new IntersectionObserver(
		(entries, observer) => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					observer.unobserve(entry.target)

					const settings = JSON.parse($map.dataset.settings)
					const apiKey = $map.dataset.apiKey
					const { center_lat: lat, center_lng: lng, zoom, marks } = settings

					ymaps
						.load(`https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=${ apiKey }&load=Map,Placemark`)
						.then(maps => {
							const map = new maps.Map($map, {
								center: [lat, lng],
								zoom,
								controls: []
							})

							marks.filter(mark => mark.type === 'Point').forEach(mark => {
								map.geoObjects.add(new maps.Placemark(mark.coords))
							})
						})
				}
			})
		}, {
			threshold: 0.1
		}
	)

	mapIO.observe($map)
}