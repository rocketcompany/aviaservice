import Swiper, { Lazy, Navigation, Pagination } from "swiper";

Swiper.use([Navigation, Lazy, Pagination])

const homeServicesSlider = new Swiper('.home-services__slider', {
	loop: false,
	spaceBetween: 20,
	slidesPerView: 1.5,
	preloadImages: false,
	lazy: true,
	grabCursor: true,
	navigation: {
		prevEl: '.home-services__navigation-prev',
		nextEl: '.home-services__navigation-next'
	},
	pagination: {
		el: '.home-services__pagination .swiper-pagination',
	},
	on: {
		afterInit: (swiper) => {
			const $homeServicesPagination = document.querySelector('.home-services__pagination')
			$homeServicesPagination.style.setProperty('--slides-count', swiper.slides.length)
		},
		beforeResize: (swiper) => {
			const $homeServicesNavigation = document.querySelector('.home-services__navigation')
			const $slideWidth = swiper.slides[0].clientWidth
			$homeServicesNavigation.style.top = `${ $slideWidth * 0.57 + 20 }px`
			$homeServicesNavigation.style.left = `${ $slideWidth - $homeServicesNavigation.clientWidth / 2 + swiper.params.spaceBetween / 2 }px`

			swiper.el.style.setProperty('--slide-content-width', `${ $slideWidth / 2 - swiper.params.spaceBetween / 2 }px`)

			const $homeServicesPagination = document.querySelector('.home-services__pagination')
			$homeServicesPagination.style.setProperty('--slides-count', swiper.slides.length)
		}
	},
	breakpoints: {
		320: {
			slidesPerView: 1
		},
		768: {
			slidesPerView: 1.5
		}
	}
})

const trustSlider = new Swiper('.trust__slider', {
	spaceBetween: 20,
	observer: true,
	navigation: {
		prevEl: '.trust__navigation-prev',
		nextEl: '.trust__navigation-next'
	},
	pagination: {
		el: '.trust__pagination .swiper-pagination'
	},
	on: {
		afterInit: swiper => {
			const $trustPagination = document.querySelector('.trust__pagination')
			$trustPagination.style.setProperty('--slides-count', swiper.slides.length)
		},
		beforeResize: swiper => {
			const $trustPagination = document.querySelector('.trust__pagination')
			$trustPagination.style.setProperty('--slides-count', swiper.slides.length)
		}
	}
})

const companyServicesSlider = new Swiper('.company-services__slider', {
	spaceBetween: 20,
	observer: true,
	observeParents: true,
	navigation: {
		prevEl: '.company-services__navigation-prev',
		nextEl: '.company-services__navigation-next'
	},
	pagination: {
		el: '.company-services__pagination .swiper-pagination'
	},
	on: {
		afterInit: swiper => {
			const $companyServicesPagination = document.querySelector('.company-services__pagination')
			$companyServicesPagination.style.setProperty('--slides-count', swiper.slides.length)
		},
		beforeResize: swiper => {
			const $companyServicesPagination = document.querySelector('.company-services__pagination')
			$companyServicesPagination.style.setProperty('--slides-count', swiper.slides.length)
		}
	}
})