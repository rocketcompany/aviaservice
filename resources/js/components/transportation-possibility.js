import Choices from "choices.js"
import axios from "axios"
import { WP_AJAX_ENDPOINT } from "../constants"
import { setSubmitLoading } from "../helpers/setSubmitLoading"

const $transportationPossibility = document.querySelector('.transportation-possibility')
const $transportationPossibilityForm = document.querySelector('.transportation-possibility-form')
const $transportationPossibilityCountry = document.getElementById('transportation-possibility-country')
const $transportationPossibilityCity = document.getElementById('transportation-possibility-city')
const $transportationPossibilityResponse = document.querySelector('.transportation-possibility-form__response')

const transportation_possibility_i18n = window.i18n.transportation_possibility

if ($transportationPossibilityCountry && $transportationPossibilityCity) {
	const transportationPossibilityCountry = new Choices($transportationPossibilityCountry, {
		noResultsText: transportation_possibility_i18n.noResultsText,
		placeholder: true,
		searchFields: ['label'],
		resetScrollPosition: false
	})

	const transportationPossibilityCity = new Choices($transportationPossibilityCity, {
		searchEnabled: false,
		loadingText: transportation_possibility_i18n.loadingText,
		placeholder: true,
		callbackOnInit() {
			this.disable()
		}
	})

	$transportationPossibilityCountry.addEventListener('choice', e => {
		transportationPossibilityCity.setChoices(async () => {
				const formData = new FormData()
				const nonce = $transportationPossibilityForm.querySelector("[name='transportation-possibility-form_nonce']").value

				formData.append('country', e.detail.choice.value)
				formData.append('transportation-possibility-form_nonce', nonce)
				formData.append('action', 'get_cities')

				try {
					const res = await axios(WP_AJAX_ENDPOINT, {
						method: 'POST',
						data: formData
					})

					return res.data.data.cities.map((city, idx) => ({ value: city, label: city, selected: idx === 0 }))
				} catch (err) {

				}
			},
			'value',
			'label',
			true
		)
	})
}

$transportationPossibilityForm?.addEventListener('submit', e => {
	e.preventDefault()

	const formData = new FormData(e.target)
	const $submit = e.target.querySelector('button[type="submit"]')
	setSubmitLoading($submit)

	axios(WP_AJAX_ENDPOINT, {
		method: 'POST',
		data: formData
	})
		.then(res => {
			const { availability } = res.data.data

			const $transportationPossibilityResponse = document.querySelector('.transportation-possibility-form__response')

			if (availability) {
				$transportationPossibilityResponse.innerHTML = `<div class="transportation-possibility-form__response--available">${ transportation_possibility_i18n.available }</div>`
			} else {
				$transportationPossibilityResponse.innerText = `<div class="transportation-possibility-form__response--available">${ transportation_possibility_i18n.unavailable }</div>`
			}
			$transportationPossibilityResponse.classList.add('_active')
		})
		.catch(err => {
			console.log(err)
		})
		.finally(() => {
			setSubmitLoading($submit, false)
		})
})

const ro = new ResizeObserver(entries => {
	for (const entry of entries) {
		const height = entry.target.offsetHeight
		const style = getComputedStyle(entry.target)
		const mt = +style.marginTop.replace('px', '')

		$transportationPossibility.style.setProperty('--height', `${ height + mt }px`)
	}
})

if ($transportationPossibilityResponse) {
	ro.observe($transportationPossibilityResponse)
}
