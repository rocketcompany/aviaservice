import * as yup from 'yup'
import axios from 'axios'
import Modal from 'bootstrap/js/dist/modal'
import { WP_AJAX_ENDPOINT } from "../constants";
import { setSubmitLoading } from "../helpers/setSubmitLoading";
import { formHandleErrors } from "../helpers/formHandleErrors";

const validateSchemaErrors = window.i18n.errors
const validateSchema = yup.object().shape({
	name: yup.string().required(validateSchemaErrors.name_required),
	phone: yup.string().required(validateSchemaErrors.phone_required),
	privacy_policy: yup.string().required(validateSchemaErrors.policy_privacy_required).matches('on'),
})

const $formValidateControls = document.querySelectorAll('.form-validate__control, .form-validate__checkbox')
$formValidateControls.forEach(el => {
	switch (el.tagName.toLowerCase()) {
		case 'input':
		case 'textarea': {
			el.addEventListener('input', e => {
				e.target.classList.remove('_invalid')

				const $error = e.target.closest('.form-row').querySelector('.form-error')
				if ($error) {
					$error.classList.remove('_active')
				}
			})
		}
	}
})

const $orderCargoForm = document.querySelector('.order-cargo-form')
$orderCargoForm?.addEventListener('submit', async e => {
	e.preventDefault()

	const $submit = e.target.querySelector('button[type="submit"]')
	const formData = new FormData(e.target)
	const data = Object.fromEntries(formData)

	const schema = validateSchema.pick(['name', 'phone', 'privacy_policy'])

	try {
		await schema.validate(data, { abortEarly: false })

		setSubmitLoading($submit)

		axios(WP_AJAX_ENDPOINT, {
			method: 'POST',
			data: formData
		})
			.then(response => {
				e.target.reset()

				const $thankYouModal = document.getElementById('thankYouModal')
				const thankYouModal = Modal.getOrCreateInstance($thankYouModal)
				thankYouModal.show()
			})
			.catch(err => {
			})
			.finally(() => {
				setSubmitLoading($submit, false)
			})
	} catch (errors) {
		formHandleErrors(e.target, errors)
	}
})

const $callbackModalForm = document.querySelector('.callback-modal-form')
$callbackModalForm?.addEventListener('submit', async e => {
	e.preventDefault()

	const $submit = e.target.querySelector('button[type="submit"]')
	const formData = new FormData(e.target)
	const data = Object.fromEntries(formData)

	const schema = validateSchema.pick(['name', 'phone', 'privacy_policy'])

	try {
		await schema.validate(data, { abortEarly: false })

		setSubmitLoading($submit)

		axios(WP_AJAX_ENDPOINT, {
			method: 'POST',
			data: formData
		})
			.then(response => {
				e.target.reset()

				const $currentModal = e.target.closest('.modal')
				const currentModal = Modal.getOrCreateInstance($currentModal)

				const $thankYouModal = document.getElementById('thankYouModal')
				const thankYouModal = Modal.getOrCreateInstance($thankYouModal)
				currentModal.hide()
				thankYouModal.show()
			})
			.catch(err => {
			})
			.finally(() => setSubmitLoading($submit, false))
	} catch (errors) {
		formHandleErrors(e.target, errors)
	}
})

const $orderTransportationModalForm = document.querySelector('.order-transportation-modal-form')
$orderTransportationModalForm?.addEventListener('submit', async e => {
	e.preventDefault()

	const $submit = e.target.querySelector('button[type="submit"]')
	const formData = new FormData(e.target)
	const data = Object.fromEntries(formData)

	const schema = validateSchema.pick(['name', 'phone', 'privacy_policy'])

	try {
		await schema.validate(data, { abortEarly: false })

		setSubmitLoading($submit)

		axios(WP_AJAX_ENDPOINT, {
			method: 'POST',
			data: formData
		})
			.then(response => {
				e.target.reset()

				setSubmitLoading($submit, false)

				const $currentModal = e.target.closest('.modal')
				const currentModal = Modal.getOrCreateInstance($currentModal)

				const $thankYouModal = document.getElementById('thankYouModal')
				const thankYouModal = Modal.getOrCreateInstance($thankYouModal)

				currentModal.hide()
				thankYouModal.show()
			})
			.catch(err => {
			})
			.finally(() => {
				setSubmitLoading($submit, false)
			})
	} catch (errors) {
		formHandleErrors(e.target, errors)
	}
})