export const setSubmitLoading = ($submit, state = true) => {
	if (!$submit) return

	if (state) {
		$submit.disabled = true
		$submit.classList.add('_loading')
	} else {
		$submit.disabled = false
		$submit.classList.remove('_loading')
	}
}