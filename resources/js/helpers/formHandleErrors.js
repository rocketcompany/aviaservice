export const formHandleErrors = (target, errors) => {
	errors.inner.forEach(error => {
		const $control = target[error.path]
		$control.classList.add('_invalid')
		const $inputError = $control.closest('.form-row').querySelector('.form-error')
		$inputError.innerText = error.message
		$inputError.classList.add('_active')
	})
}