<?php

require_once "vendor/autoload.php";

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

define('ASSETS_PATH_URI', get_template_directory_uri() . '/assets');
define('ASSETS_PATH', get_template_directory() . '/assets');
define('FRONT_PAGE_ID', intval(get_option('page_on_front')));

$logger = new Logger('air');
$logger->pushHandler(new StreamHandler(get_template_directory() . '/logs/debug.log', Logger::DEBUG));

require_once 'includes/acf-settings.php';
require_once 'includes/ajax.php';
require_once 'includes/cpt.php';
require_once 'includes/theme-functions.php';
require_once 'includes/theme-settings.php';

add_action('wp_enqueue_scripts', function () {
	wp_enqueue_style(
		'style',
		ASSETS_PATH_URI . '/css/main.css',
		[],
		filemtime(ASSETS_PATH . '/css/main.css')
	);

	wp_enqueue_script(
		'script',
		ASSETS_PATH_URI . '/js/app.js',
		[],
		filemtime(ASSETS_PATH . '/js/app.js'),
		true
	);

	wp_localize_script('script', 'i18n', [
		'errors' => [
			'name_required' => __('Введите ваше имя', 'air'),
			'phone_required' => __('Введите ваш номер телефона', 'air'),
			'policy_privacy_required' => __('Вы должны согласиться с обработкой персональных данных', 'air')
		],
		'transportation_possibility' => [
			'loadingText' => __('Загрузка...', 'air'),
			'noResultsText' => __('Ничего не найдено', 'air'),
			'available' => __('Перевозка в нужный Вам регион осуществляется', 'air'),
			'unavailable' => __('Перевозка в нужный Вам регион не осуществляется', 'air')
		]
	]);
});

add_action('admin_enqueue_scripts', function () {
	wp_enqueue_style(
		'air-admin',
		ASSETS_PATH_URI . '/css/admin.css',
		[],
		filemtime(ASSETS_PATH . '/css/admin.css')
	);
});