<?php get_header() ?>

<?php
$post_type_obj = get_post_type_object('service');

$title = get_the_title();
$hero = get_field('hero');
$bgi = wp_get_attachment_image_url($hero['background_image'], 'full');
?>

	<main class="main">
		<div class="service-single-hero" style="--bgi: url(<?= $bgi ?>) ">
			<div class="service-single-hero__grid">
				<div class="service-single-hero__left"></div>

				<div class="service-single-hero__right"></div>
			</div>

			<div class="container service-single-hero__container">
				<div class="breadcrumbs breadcrumbs--service-single">
					<ul class="breadcrumbs__list">
						<li class="breadcrumbs__item">
							<a href="<?= home_url() ?>" class="breadcrumbs__link">
								<?= __('Главная', 'air') ?>
							</a>
						</li>
						<li class="breadcrumbs__item">
							<a href="<?= get_post_type_archive_link('service') ?>" class="breadcrumbs__link">
								<?= $post_type_obj->labels->name ?>
							</a>
						</li>
						<li class="breadcrumbs__item">
							<div class="breadcrumbs__current">
								<?= $title ?>
							</div>
						</li>
					</ul>
				</div>

				<div class="service-single-hero__content">
					<h1 class="service-single-hero__title h1">
						<?= $hero['title'] ?>
					</h1>

					<div class="service-single-hero__subtitle">
						<?= $hero['subtitle'] ?>
					</div>

					<div class="service-single-hero__image">
						<?= wp_get_attachment_image($hero['background_image'], 'large') ?>
					</div>

					<div class="service-single-hero__text">
						<?= $hero['text'] ?>
					</div>

					<div class="service-single-hero__cta">
						<button type="button" class="button button--primary service-single-hero__button"
								data-bs-toggle="modal"
								data-bs-target="#modalOrderTransportation"
						>
							<?= __('Заказать', 'air') ?>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="service-single__text-editor text-editor text-editor--service-single">
			<div class="container text-editor__container">
				<div class="text-editor__inner">
					<?php the_content() ?>
				</div>
			</div>
		</div>

		<?php get_template_part('template-parts/sections/transportation-possibility', null, ['classes' => 'transportation-possibility--service-single']) ?>

		<?php get_template_part('template-parts/sections/order-cargo', null, ['classes' => 'order-cargo--service-single']) ?>
	</main>

<?php get_footer() ?>