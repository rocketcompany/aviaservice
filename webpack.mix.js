const mix = require('laravel-mix')

mix
	.sourceMaps(false, 'source-map')
	.js('resources/js/app.js', 'assets/js')
	.sass('resources/scss/main.scss', 'assets/css')
	.sass('resources/scss/admin.scss', 'assets/css')
	.copyDirectory('resources/images', 'assets/images')
	.copyDirectory('resources/fonts', 'assets/fonts')
	.options({
		processCssUrls: false
	})