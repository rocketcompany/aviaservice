const { src, dest, parallel, series, watch } = require('gulp');
const svgSprite = require('gulp-svg-sprite');

const svgSprites = () => {
	return src('./resources/sprite/**.svg')
		.pipe(
			svgSprite({
				mode: {
					stack: {
						sprite: '../sprite.svg',
					},
				},
			})
		)
		.pipe(dest('./assets/'));
};

const watchFiles = () => {
	watch('./resources/sprite/**.svg', svgSprites);
};

exports.default = series(parallel(svgSprites), watchFiles)

exports.build = series(parallel(svgSprites))