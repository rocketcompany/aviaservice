<?php
$footer = get_field('footer', 'option');
$contacts = get_field('contacts', 'option');
$integrations = get_field('integrations', 'option');
?>

<footer class="footer">
	<div class="container footer__container">
		<div class="footer__grid">
			<div class="footer__column">
				<a href="/" class="footer__logo">
					<?= wp_get_attachment_image($footer['logo'], 'medium') ?>
				</a>

				<div class="footer__text">
					<?= $footer['text'] ?>
				</div>

				<button type="button" class="footer__callback-button footer-callback-button" data-bs-toggle="modal"
						data-bs-target="#modalCallback"
				>
					<span class="footer-callback-button__icon">
						<svg>
							<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
						</svg>
					</span>
					<?= __('Заказать звонок', 'air') ?>
				</button>
			</div>

			<div class="footer__column">
				<div class="footer__column-title footer__column-title--collapse h4">
					<?= __('Меню', 'air') ?>
				</div>

				<div class="collapse footer__collapse">
					<div class="footer__menu footer-menu">
						<?php
						wp_nav_menu([
							'theme_location' => 'footer_menu',
							'container' => false,
							'menu_class' => 'footer-menu__list',
							'depth' => 1
						]);
						?>
					</div>
				</div>
			</div>

			<div class="footer__column">
				<div class="footer__column-title footer__column-title--collapse h4">
					<?= __('Услуги', 'air') ?>
				</div>

				<div class="collapse footer__collapse">
					<div class="footer__menu footer-menu">
						<ul class="footer-menu__list">
							<?php foreach ($footer['services'] as $service): ?>
								<li class="footer-menu__item">
									<a href="<?= get_the_permalink($service) ?>" class="footer-menu__link">
										<?= get_the_title($service) ?>
									</a>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="footer__column">
				<div class="footer__column-title h4">
					<?= __('Контакты', 'air') ?>
				</div>

				<div class="footer__contacts footer-contacts">
					<ul class="footer-contacts__list">
						<li class="footer-contacts__item">
							<a href="tel:<?= sanitize_phone($contacts['phone']) ?>" class="footer-contacts__link">
								<span class="footer-contacts__icon">
									<svg>
										<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
									</svg>
								</span>

								<span class="footer-contacts__link-text">
									<?= $contacts['phone'] ?>
								</span>
							</a>
						</li>

						<li class="footer-contacts__item">
							<a href="mailto:<?= $contacts['email'] ?>" class="footer-contacts__link">
								<span class="footer-contacts__icon">
									<svg>
										<use xlink:href="<?= get_sprite_uri() ?>#email"></use>
									</svg>
								</span>

								<span class="footer-contacts__link-text">
									<?= $contacts['email'] ?>
								</span>
							</a>
						</li>

						<?php if ($integrations['instagram']['enabled']): ?>
							<li class="footer-contacts__item">
								<a href="<?= $contacts['social']['instagram']['url'] ?>" class="footer-contacts__link"
								   target="_blank"
								>
								<span class="footer-contacts__icon">
									<svg>
										<use xlink:href="<?= get_sprite_uri() ?>#instagram"></use>
									</svg>
								</span>

									<span class="footer-contacts__link-text">
									<?= $contacts['social']['instagram']['title'] ?>
								</span>
								</a>
							</li>
						<?php endif ?>

						<li class="footer-contacts__item">
							<div class="footer-contacts__text">
								<span class="footer-contacts__icon">
									<svg>
										<use xlink:href="<?= get_sprite_uri() ?>#map-marker"></use>
									</svg>
								</span>
								<?= $contacts['address'] ?>
							</div>
						</li>
					</ul>
				</div>

				<div class="footer__contacts-mobile footer-contacts-mobile">
					<a href="tel:<?= sanitize_phone($contacts['phone']) ?>" class="footer-contacts-mobile__phone">
						<?= $contacts['phone'] ?>
					</a>

					<ul class="footer-contacts-mobile__list">
						<li class="footer-contacts-mobile__item">
							<a href="<?= $contacts['social']['instagram']['url'] ?>"
							   class="footer-contacts-mobile__link"
							   target="_blank"
							>
								<svg>
									<use xlink:href="<?= get_sprite_uri() ?>#instagram"></use>
								</svg>
							</a>
						</li>

						<li class="footer-contacts-mobile__item">
							<a href="mailto:<?= $contacts['email'] ?>" class="footer-contacts-mobile__link">
								<svg>
									<use xlink:href="<?= get_sprite_uri() ?>#email"></use>
								</svg>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="footer__copyright">
			<div class="footer__copyright-text">
				<?= $footer['copyright'] ?>
			</div>
			<a href="https://rocketcompany.website" class="rc-logo" target="_blank">
				<img src="<?= ASSETS_PATH_URI ?>/images/logo-rocket.svg" alt="rocket company" loading="lazy" width="84"
					 height="29"
				>
			</a>
		</div>
	</div>
</footer>
</div> <!-- /.wrapper -->

<?php get_template_part('template-parts/modals/modals') ?>
<?php get_template_part('template-parts/offcanvas') ?>

<?php wp_footer(); ?>

</body>
</html>