<?php

class TransportationPossibility
{
	static function ajax_get_cities()
	{
		$country = $_POST['country'];

		if (!$country || !wp_verify_nonce($_POST["transportation-possibility-form_nonce"], "transportation-possibility-form")) {
			wp_send_json_error([], 400);
		}

		global $wpdb;
		$result = $wpdb->get_results($wpdb->prepare("SELECT name FROM {$wpdb->prefix}city WHERE country_id=%d ORDER BY name", $country));

		$cities = array_map(function ($item) {
			return $item->name;
		}, $result);

		wp_send_json_success(['cities' => $cities]);
	}

	static function get_countries()
	{
		global $wpdb;
		return $wpdb->get_results("SELECT * FROM {$wpdb->prefix}country ORDER BY name");
	}

	static function ajax_check_transport_possibility()
	{
		$country = $_POST['country'];
		$city = $_POST['city'];

		if (!$country || !$city || !wp_verify_nonce($_POST["transportation-possibility-form_nonce"], "transportation-possibility-form")) {
			wp_send_json_error([], 400);
		}

		global $wpdb;
		$result = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->prefix}city WHERE `country_id`=%d AND `name`=%s", $country, $city));
		foreach ($result as $item) {
			if ($item->available_kaz || $item->available_msk) {
				wp_send_json_success(['availability' => true]);
			}
		}

		wp_send_json_success(['availability' => false]);
	}

	static function render_page()
	{
		global $wpdb;
		$prefix = $wpdb->prefix;
		$rows = $wpdb->get_results("SELECT {$prefix}city.id, {$prefix}city.name, {$prefix}city.code, {$prefix}city.available_msk, {$prefix}city.available_kaz, {$prefix}country.name as country from {$prefix}city join {$prefix}country on {$prefix}city.country_id={$prefix}country.id order by {$prefix}city.name");

		get_template_part('template-parts/admin/transportation-possibility-page', null, compact('rows'));
	}
}