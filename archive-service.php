<?php get_header() ?>

<?php
$services_settings = get_field('services_settings', 'option');
$hero = $services_settings['hero'];
$bgi_tablet = wp_get_attachment_image_url($hero['background_image_tablet'], 'full');

$features = $services_settings['features'];
?>

	<main class="main">
		<div class="services-hero" style="--bgi-tablet: url(<?= $bgi_tablet ?>)">
			<div class="services-hero__grid">
				<div class="services-hero__left"></div>

				<div class="services-hero__right"></div>
			</div>

			<div class="container services-hero__container">
				<div class="breadcrumbs breadcrumbs--services-archive">
					<ul class="breadcrumbs__list">
						<li class="breadcrumbs__item">
							<a href="<?= home_url() ?>" class="breadcrumbs__link">
								<?= __('Главная', 'air') ?>
							</a>
						</li>
						<li class="breadcrumbs__item">
							<div class="breadcrumbs__current">
								<?= post_type_archive_title(); ?>
							</div>
						</li>
					</ul>
				</div>

				<div class="services-hero__content">
					<h1 class="services-hero__title h1">
						<?= $hero['title'] ?>
					</h1>

					<h2 class="services-hero__subtitle h1">
						<?= $hero['subtitle'] ?>
					</h2>

					<div class="services-hero__image">
						<?= wp_get_attachment_image($hero['background_image_mobile'], 'large', false, ['loading' => false]) ?>
					</div>

					<div class="services-hero__text">
						<?= $hero['text'] ?>
					</div>
				</div>
			</div>
		</div>

		<section class="company-services">
			<div class="container company-services__container">
				<h2 class="company-services__title">
					<?= __('Услуги компании', 'air') ?>
				</h2>

				<div class="company-services__list">
					<?php foreach ($wp_query->posts as $service): ?>
						<div class="company-services__item company-service">
							<div class="company-service__grid">
								<div class="company-service__left">
									<div class="company-service__image ratio ratio-16x9">
										<?= wp_get_attachment_image(get_field('image', $service->ID), 'medium_large') ?>
									</div>
								</div>

								<div class="company-service__right">
									<div class="company-service__content">
										<div class="company-service__name">
											<?= get_the_title($service->ID) ?>
										</div>

										<div class="company-service__description">
											<?= get_field('description', $service->ID) ?>
										</div>
									</div>

									<a href="<?= get_the_permalink($service->ID) ?>" class="company-service__link">
										<span class="company-service__link-text">
											Детальнее
										</span>

										<span class="company-service__link-icon">
											<svg>
												<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
											</svg>
										</span>
									</a>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				</div>

				<div class="company-services__carousel">
					<div class="company-services__slider swiper">
						<div class="swiper-wrapper">
							<?php foreach ($wp_query->posts as $service): ?>
								<div class="swiper-slide">
									<div class="company-service-slide">
										<div class="company-service-slide__image ratio ratio-29x15">
											<?= wp_get_attachment_image(get_field('image', $service->ID), 'large') ?>
										</div>

										<div class="company-service-slide__name">
											<?= get_the_title($service->ID) ?>
										</div>

										<div class="company-service-slide__description">
											<?= get_field('description', $service->ID) ?>
										</div>

										<a href="<?= get_the_permalink($service->ID) ?>" class="company-service__link">
											<span class="company-service__link-text">
												<?= __('Детальнее', 'air') ?>
											</span>

											<span class="company-service__link-icon">
												<svg>
													<use xlink:href="/assets/sprite.svg#arrow-right"
													></use>
												</svg>
											</span>
										</a>
									</div>
								</div>
							<?php endforeach ?>
						</div>
					</div>

					<div class="company-services__navigation">
						<button type="button" class="button-slider-navigation company-services__navigation-prev">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-left"></use>
							</svg>
						</button>

						<button type="button" class="button-slider-navigation company-services__navigation-next">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
							</svg>
						</button>
					</div>

					<div class="as-slider__pagination company-services__pagination">
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</section>

		<section class="company-features">
			<div class="container company-features__container">
				<h2 class="company-features__title h2">
					<?= $features['title'] ?>
				</h2>

				<div class="company-features__grid">
					<div class="company-features__column">
						<ul class="company-features__list">
							<?php foreach ($features['list'] as $item): ?>
								<li class="company-features__item">
									<?= $item['text'] ?>
								</li>
							<?php endforeach ?>
						</ul>

						<button class="company-features__read-more">
							<?= __('Читать весь текст', 'air') ?>
						</button>
					</div>

					<div class="company-features__column">
						<div class="company-features__image">
							<?= wp_get_attachment_image($features['image'], 'large') ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/sections/order-cargo') ?>
	</main>

<?php get_footer() ?>