<?php
$header = get_field('header', 'option');
$contacts = get_field('contacts', 'option');
$integrations = get_field('integrations', 'option');
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>

	<?php
	if (!empty($integrations['yandex_metrika'])) {
		echo $integrations['yandex_metrika'];
	}
	?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div class="wrapper">
	<header class="header">
		<div class="container header__container">
			<div class="header__grid">
				<div class="header__right">
					<a href="<?= home_url() ?>" class="header__logo logo">
						<?= wp_get_attachment_image($header['logo'], 'medium', false, ['loading' => false]) ?>
					</a>
				</div>

				<div class="header__left">
					<nav class="header__navigation header-navigation">
						<?php
						wp_nav_menu([
							'theme_location' => 'header_menu',
							'container' => false,
							'menu_class' => 'header-navigation__list',
							'depth' => 1
						]);
						?>
					</nav>

					<button type="button" class="header__phone" data-bs-toggle="modal" data-bs-target="#modalCallback">
						<svg>
							<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
						</svg>
					</button>

					<?php get_template_part('template-parts/language-switcher') ?>

					<button type="button" class="header__bars" data-bs-toggle="offcanvas"
							data-bs-target="#offcanvasMenu"
					>
						<svg>
							<use xlink:href="<?= get_sprite_uri() ?>#bars"></use>
						</svg>
					</button>
				</div>
			</div>
		</div>
	</header>
