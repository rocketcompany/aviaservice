<?php

function get_sprite_uri()
{
	return get_template_directory_uri() . '/assets/sprite.svg';
}

function sanitize_phone($phone)
{
	return str_replace(array('-', ' ', '(', ')', '+'), '', $phone);
}