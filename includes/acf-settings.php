<?php

add_filter('acf/settings/save_json', function ($path) {
	return get_stylesheet_directory() . '/includes/acf-json';
});

add_filter('acf/settings/show_admin', function ($show_admin) {
	return true;
});

require_once "acf-ym/acf-yandex-map.php";

if (function_exists('acf_add_options_page')) {
	acf_add_options_page([
		'page_title' => 'Настройки темы',
		'menu_title' => 'Настройки темы',
		'menu_slug' => 'theme-general-settings',
		'capability' => 'edit_posts'
	]);

	if (function_exists('acf_add_options_sub_page')) {
		acf_add_options_sub_page([
			'page_title' => 'Настройки услуги',
			'menu_title' => 'Настройки',
			'parent_slug' => 'edit.php?post_type=service',
			'menu_slug' => 'services-settings'
		]);
	}
}
