<?php

add_action(
	'after_setup_theme',
	function () {
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		register_nav_menus([
			'header_menu' => 'Header Menu',
			'mobile_menu' => 'Mobile Menu',
			'footer_menu' => 'Footer Menu'
		]);

		load_theme_textdomain('air', get_template_directory() . '/languages');

		// remove SVG and global styles
		remove_action('wp_enqueue_scripts', 'wp_enqueue_global_styles');

		// remove wp_footer actions which add's global inline styles
		remove_action('wp_footer', 'wp_enqueue_global_styles', 1);

		// remove render_block filters which adding unnecessary stuff
		remove_filter('render_block', 'wp_render_duotone_support');
		remove_filter('render_block', 'wp_restore_group_inner_container');
		remove_filter('render_block', 'wp_render_layout_support_flag');
	},
	10,
	0
);

add_action('admin_menu', function () {
	add_menu_page(
		'Доставка по городам',
		'Доставка по городам',
		'manage_options',
		'transportation-possibility',
		['TransportationPossibility', 'render_page'],
		'dashicons-admin-home',
		6
	);
});

//remove emoji support
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Remove rss feed links
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);

// remove wp-embed
add_action('wp_footer', function () {
	wp_dequeue_script('wp-embed');
});

add_action('wp_enqueue_scripts', function () {
	// remove block library css
	wp_dequeue_style('wp-block-library');
	// remove comment reply JS
	wp_dequeue_script('comment-reply');
});

add_filter(
	'nav_menu_css_class',
	function ($classes, $item, $args, $depth) {
		switch ($args->theme_location) {
			case 'header_menu':
				$classes[] = 'header-navigation__item';
				return $classes;

			case 'mobile_menu':
				$classes[] = 'offcanvas-menu-navigation__item';
				return $classes;

			case 'footer_menu':
				$classes[] = 'footer-menu__item';
				return $classes;

			default:
				return $classes;
		}
	},
	10,
	4
);

add_filter(
	'nav_menu_link_attributes',
	function ($atts, $item, $args, $depth) {
		switch ($args->theme_location) {
			case 'header_menu':
				$atts['class'] = 'header-navigation__link';
				return $atts;

			case 'mobile_menu':
				$atts['class'] = 'offcanvas-menu-navigation__link';
				return $atts;

			case 'footer_menu':
				$atts['class'] = 'footer-menu__link';
				return $atts;

			default:
				return $atts;
		}
	},
	10,
	4
);

add_action('pre_get_posts', function ($query) {
	if (!is_admin() && $query->is_main_query() && is_post_type_archive('service')) {
		$query->set('orderby', 'menu_order');
	}
});

add_filter(
	'avia_sbi_class',
	function ($class, $feed_id) {
		switch ($feed_id) {
			case '*1':
				return '';

			default:
				return $class;
		}
	},
	10,
	2
);

add_filter(
	'avia_sbi_wrap_class',
	function ($class, $feed_id) {
		switch ($feed_id) {
			case '*1':
				return 'instagram-feed__posts';

			case '*2':
				return 'thank-you-modal-instagram-feed__grid';

			default:
				return $class;
		}
	},
	10,
	2
);