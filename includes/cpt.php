<?php

add_action('init', function () {
	// услуги
	register_post_type(
		'service',
		[
			'labels' => [
				'name' => __('Услуги', 'air'),
				'singular_name' => __('Услуга', 'air'),
				'add_new' => __('Добавить услугу', 'air'),
				'add_new_item' => __('Добавить услугу', 'air'),
				'edit_item' => __('Редактировать услугу', 'air'),
				'all_items' => __('Все услуги', 'air')
			],
			'public' => true,
			'has_archive' => true,
			'rewrite' => ['slug' => 'services'],
			'supports' => ['title', 'editor'],
			'menu_icon' => 'dashicons-money-alt',
		]
	);

	// контактные формы
	register_post_type(
		'contact-form',
		[
			'labels' => [
				'name' => 'Контактные формы',
				'singular_name' => 'Контактная форма',
				'add_new' => 'Добавить форму',
				'add_new_item' => 'Добавить форму',
				'edit_item' => 'Редактировать форму',
				'all_items' => 'Все формы'
			],
			'public' => false,
			'supports' => ['title'],
			'menu_icon' => 'dashicons-email',
			'show_ui' => true,
		]
	);
});