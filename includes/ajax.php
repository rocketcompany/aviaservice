<?php

add_action('wp_ajax_nopriv_contact_form', 'ajax_contact_form');
add_action('wp_ajax_contact_form', 'ajax_contact_form');

function ajax_contact_form()
{
	$form = $_POST['form'];

	if (!$form || !wp_verify_nonce($_POST["{$form}_form_nonce"], "{$form}_form")) {
		wp_send_json_error([], 400);
	}

	$required_form_fields = [];
	$form_fields = [];
	$form_fields_description = [
		'form' => 'Форма',
		'name' => 'Имя',
		'phone' => 'Телефон',
		'from' => 'Откуда вывозить груз',
		'where' => 'Куда отправлять груз',
		'weight' => 'Все груза',
		'commentary' => 'Комментарий'
	];
	$form_names = [
		'callback-modal' => 'Обратный звонок',
		'order-cargo' => 'Заказать международную грузоперевозку',
		'order-transportation-modal' => 'Заказать грузоперевозку'
	];
	$email_message = '';

	switch ($form) {
		case 'callback-modal':
			$required_form_fields = ['name', 'phone'];
			$form_fields = ['name', 'phone'];
			break;

		case 'order-cargo':
		case 'order-transportation-modal':
			$required_form_fields = ['name', 'phone'];
			$form_fields = ['name', 'phone', 'from', 'where', 'weight', 'commentary'];
			break;

		default:
			wp_send_json_error([], 400);
	}

	$errors = [];
	foreach ($required_form_fields as $field) {
		if (empty($_POST[$field])) {
			$errors[] = ['message' => sprintf("%s %s", $form_fields_description[$field], 'обязательное поле')];
		}
	}

	if (!isset($_POST['privacy_policy'])) {
		$errors[] = ['message' => 'Вы должны согласиться с политикой персональных данных'];
	}

	if ($errors) {
		wp_send_json_error(['errors' => $errors], 400);
	}

	$created_post_id = wp_insert_post(wp_slash([
		'post_status' => 'publish',
		'post_type' => 'contact-form',
		'post_title' => sprintf("[%s] %s (%s)", $form_names[$form], $_POST['name'], $_POST['phone'])
	]));

	foreach (array_merge(['form'], $form_fields) as $field) {
		update_field($field, $_POST[$field], $created_post_id);

		$email_message .= sprintf(
			"%s: %s %s",
			$form_fields_description[$field],
			$field === 'form' ? $form_names[$form] : $_POST[$field],
			PHP_EOL
		);
	}

	$email_subject = sprintf('Avia Service - Контактная форма [%s] %s (%s)', $form_names[$form], $_POST['name'], $_POST['phone']);
	wp_mail(get_option('admin_email'), $email_subject, $email_message);

	wp_send_json_success([array_merge(['form'], $form_fields)]);
}

add_action('wp_ajax_nopriv_get_cities', ['TransportationPossibility', 'ajax_get_cities']);
add_action('wp_ajax_get_cities', ['TransportationPossibility', 'ajax_get_cities']);

add_action('wp_ajax_nopriv_check_transport_possibility', ['TransportationPossibility', 'ajax_check_transport_possibility']);
add_action('wp_ajax_check_transport_possibility', ['TransportationPossibility', 'ajax_check_transport_possibility']);