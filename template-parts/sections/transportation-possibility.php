<?php
$transportation_possibility = get_field('transportation_possibility', FRONT_PAGE_ID);
if (!$transportation_possibility['enabled']) return;

$countries = TransportationPossibility::get_countries();
?>

<section class="transportation-possibility <?= isset($args['classes']) ? $args['classes'] : '' ?>">
	<div class="container transportation-possibility__container">
		<form class="transportation-possibility__form transportation-possibility-form">
			<h3 class="transportation-possibility-form__title h3">
				<?= __('Проверьте возможность перевозки груза в желаемую страну', 'air') ?>
			</h3>

			<div class="transportation-possibility-form__fields">
				<div class="transportation-possibility-form__row">
					<label class="transportation-possibility-form__label">
						<?= __('Страна назначения', 'air') ?>
					</label>

					<div class="transportation-possibility-form__select">
						<select name="country" id="transportation-possibility-country" class="_select"
								autocomplete="off"
						>
							<option value=""><?= __('Введите страну', 'air') ?></option>
							<?php foreach ($countries as $country): ?>
								<option value="<?= $country->id ?>">
									<?= $country->name ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="transportation-possibility-form__row">
					<label class="transportation-possibility-form__label">
						<?= __('Город назначения', 'air') ?>
					</label>

					<div class="transportation-possibility-form__select">
						<select name="city" id="transportation-possibility-city" class="_select"
								autocomplete="off"
						>
							<option value=""><?= __('Введите сначала страну', 'air') ?></option>
						</select>
					</div>
				</div>

				<div class="transportation-possibility-form__row">
					<?php wp_nonce_field('transportation-possibility-form', 'transportation-possibility-form_nonce') ?>
					<input type="hidden" name="action" value="check_transport_possibility">

					<button type="submit"
							class="button button--primary transportation-possibility-form__submit"
					>
						<?= __('Проверить', 'air') ?>
					</button>
				</div>
			</div>

			<div class="transportation-possibility-form__response"></div>
		</form>
	</div>
</section>