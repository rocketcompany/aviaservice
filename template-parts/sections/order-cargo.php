<div class="order-cargo <?= isset($args['classes']) ? $args['classes'] : '' ?>">
	<div class="container order-cargo__container">
		<form class="order-cargo__form order-cargo-form form-validate">
			<h3 class="order-cargo-form__title h3">
				<?= __('Заказать международную грузоперевозку', 'air') ?>
			</h3>

			<div class="order-cargo-form__grid">
				<div class="order-cargo-form__left">
					<div class="order-cargo-form__row form-row">
						<input type="text" name="name"
							   class="form-control order-cargo-form__form-control form-validate__control"
							   placeholder="<?= __('Ваше имя', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="order-cargo-form__row form-row">
						<input type="text" name="phone"
							   class="form-control order-cargo-form__form-control form-validate__control"
							   placeholder="<?= __('Ваш номер телефона', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="order-cargo-form__row form-row">
						<input type="text" name="from"
							   class="form-control order-cargo-form__form-control form-validate__control"
							   placeholder="<?= __('Откуда вывозить груз', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="order-cargo-form__row form-row">
						<input type="text" name="where"
							   class="form-control order-cargo-form__form-control form-validate__control"
							   placeholder="<?= __('Куда отправлять груз', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>
				</div>

				<div class="order-cargo-form__right">
					<div class="order-cargo-form__row form-row">
						<input type="text" name="weight"
							   class="form-control order-cargo-form__form-control form-validate__control"
							   placeholder="<?= __('Вес груза', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="order-cargo-form__row order-cargo-form__row--textarea form-row">
						<textarea name="commentary"
								  class="form-control order-cargo-form__form-control order-cargo-form__textarea form-validate__control"
								  placeholder="<?= __('Ваш комментарий', 'air') ?>"
						></textarea>

						<div class="form-error"></div>
					</div>
				</div>
			</div>

			<div class="order-cargo-form__row form-row">
				<div class="form-checkbox">
					<label class="form-checkbox__label">
						<input type="checkbox" name="privacy_policy"
							   class="form-checkbox__input form-validate__control visually-hidden"
						>

						<span class="form-checkbox__text">
							<?= __('Согласен с обработкой персональных данных', 'air') ?>
						</span>
					</label>
				</div>

				<div class="form-error"></div>
			</div>

			<?php wp_nonce_field('order-cargo_form', 'order-cargo_form_nonce') ?>
			<input type="hidden" name="action" value="contact_form">
			<input type="hidden" name="form" value="order-cargo">

			<button type="submit" class="button button--primary order-cargo-form__submit">
				<?= __('Заказать перевозку', 'air') ?>
			</button>
		</form>
	</div>
</div>