<?php
$languages = wpml_get_active_languages_filter(['skip_missing' => false, 'order' => 'asc', 'orderby' => 'code']);
$languages = apply_filters('wpml_active_languages', NULL, ['skip_missing' => false, 'order' => 'asc', 'orderby' => 'code']);
$current_language_code = apply_filters('wpml_current_language', NULL);
?>

<div class="header__language-switcher language-switcher">
	<ul class="language-switcher__list">
		<?php foreach ($languages as $language): ?>
			<?php if ($current_language_code === $code = $language['language_code']): ?>
				<li class="language-switcher__item">
					<div class="language-switcher__current">
						<?= $code ?>
					</div>
				</li>
			<?php else: ?>
				<li class="language-switcher__item">
					<a href="<?= $language['url'] ?>" class="language-switcher__link">
						<?= $code ?>
					</a>
				</li>
			<?php endif ?>
		<?php endforeach ?>
	</ul>
</div>
