<?php
$languages = wpml_get_active_languages_filter(['skip_missing' => false, 'orderby' => 'code']);
$current_language_code = apply_filters('wpml_current_language', NULL);
$active_language = $languages[$current_language_code];
$other_languages = array_filter($languages, function ($item) {
	return !$item['active'];
});
?>

<div class="offcanvas-menu__language-switcher offcanvas-menu-language-switcher">
	<div class="dropdown offcanvas-menu-language-switcher__dropdown">
		<button class="dropdown-toggle offcanvas-menu-language-switcher__dropdown-toggle"
				type="button"
				data-bs-toggle="dropdown" aria-expanded="false"
		>
			<span class="offcanvas-menu-language-switcher__flag">
				<img src="<?= $active_language['country_flag_url'] ?>" alt="">
			</span>
			<?= $active_language['language_code'] ?>
		</button>
		<ul class="dropdown-menu offcanvas-menu-language-switcher__dropdown-menu">
			<?php foreach ($other_languages as $language): ?>
				<li>
					<a href="<?= $language['url'] ?>"
					   class="dropdown-item offcanvas-menu-language-switcher__dropdown-item"
					>
						<span class="offcanvas-menu-language-switcher__flag">
							<img src="<?= $language['country_flag_url'] ?>" alt="">
						</span>
						<?= $language['language_code'] ?>
					</a>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>
