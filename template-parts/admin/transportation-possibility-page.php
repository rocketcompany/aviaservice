<?php
$rows = $args['rows'];
?>

<div class="wrap">
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
		<tr>
			<th>Город</th>
			<th>Код</th>
			<th>Страна</th>
			<th>Из Казани</th>
			<th>Из Москвы</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($rows as $row): ?>
			<?php
			$availability_kaz = $row->available_kaz ? '<span class="transportation-possibility__status-available">Да</span>' : '<span class="transportation-possibility__status-unavailable">Нет</span>';
			$availability_msk = $row->available_msk ? '<span class="transportation-possibility__status-available">Да</span>' : '<span class="transportation-possibility__status-unavailable">Нет</span>';
			?>
			<tr>
				<td><?= $row->name ?></td>
				<td><?= $row->code ?></td>
				<td><?= $row->country ?></td>
				<td><?= $availability_kaz ?></td>
				<td><?= $availability_msk ?></td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
</div>
