<div class="modal fade as-modal callback-modal" id="modalCallback" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog callback-modal__dialog">
		<div class="modal-content callback-modal__content">
			<div class="callback-modal__header">
				<div class="callback-modal__title h3">
					<?= __('Заказать звонок', 'air') ?>
				</div>

				<button type="button" class="as-modal__close" data-bs-dismiss="modal" aria-label="Close">
					<svg>
						<use xlink:href="<?= get_sprite_uri() ?>#times"></use>
					</svg>
				</button>
			</div>

			<div class="modal-body callback-modal__body">
				<form class="callback-modal__form callback-modal-form form-validate">
					<div class="callback-modal-form__description">
						<?= __('Мы свяжемся с Вами и ответим на любые возникшие вопросы', 'air') ?>
					</div>

					<div class="callback-modal-form__row form-row">
						<input type="text" name="name"
							   class="callback-modal-form__control form-control form-validate__control"
							   placeholder="<?= __('Ваше имя', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="callback-modal-form__row form-row">
						<input type="tel" name="phone"
							   class="callback-modal-form__control form-control form-validate__control"
							   placeholder="<?= __('Ваш номер телефона', 'air') ?>"
						>

						<div class="form-error"></div>
					</div>

					<div class="callback-modal-form__row form-row">
						<div class="form-checkbox">
							<label class="form-checkbox__label">
								<input type="checkbox" name="privacy_policy"
									   class="form-checkbox__input form-validate__control visually-hidden"
								>

								<span class="form-checkbox__text">
									<?= __('Согласен с обработкой персональных данных', 'air') ?>
								</span>
							</label>
						</div>

						<div class="form-error"></div>
					</div>

					<?php wp_nonce_field('callback-modal_form', 'callback-modal_form_nonce') ?>
					<input type="hidden" name="action" value="contact_form">
					<input type="hidden" name="form" value="callback-modal">

					<button type="submit" class="callback-modal-form__submit button button--primary button--full">
						<?= __('Ожидать звонок', 'air') ?>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>