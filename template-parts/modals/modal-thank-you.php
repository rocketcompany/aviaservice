<?php
$contacts = get_field('contacts', 'option');
$modals_settings = get_field('modals', 'option');
$integrations = get_field('integrations', 'option');
?>

<div class="modal fade as-modal thank-you-modal" id="thankYouModal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog thank-you-modal__dialog">
		<div class="modal-content thank-you-modal__content">
			<div class="thank-you-modal__header">
				<div class="thank-you-modal__header-title">
					<?= __('Спасибо за обращение!', 'air') ?>
				</div>

				<div class="thank-you-modal__header-subtitle">
					<?= __('Заявка успешно отправлена!', 'air') ?>
				</div>

				<div class="thank-you-modal__header-description">
					<?= $modals_settings['thank_you_modal']['description'] ?>
				</div>

				<button type="button" class="as-modal__close" data-bs-dismiss="modal" aria-label="Close">
					<svg>
						<use xlink:href="<?= get_sprite_uri() ?>#times"></use>
					</svg>
				</button>
			</div>

			<?php if ($integrations['instagram']['enabled']): ?>
				<div class="modal-body thank-you-modal__body">
					<div class="thank-you-modal__body-description">
						<?= __('Смотрите как мы решаем задачи клиентов', 'air') ?>
					</div>

					<div class="thank-you-modal__instagram-feed thank-you-modal-instagram-feed">
						<?= do_shortcode('[instagram-feed feed=2]') ?>

						<a href="<?= $contacts['social']['instagram']['url'] ?>"
						   class="button button--secondary thank-you-modal-instagram-feed__see-more"
						>
						<span class="button__icon button__icon--left instagram-feed__link-icon1">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#instagram1"></use>
							</svg>
						</span>
							<?= __('Смотреть больше', 'air') ?>
							<span class="button__icon button__icon--right instagram-feed__link-icon2">
							<svg>
								<use xlink:href="<?= get_sprite_uri() ?>#arrow-right"></use>
							</svg>
						</span>
						</a>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>