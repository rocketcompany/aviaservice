<div class="modal fade as-modal order-transportation-modal" id="modalOrderTransportation" tabindex="-1"
	 aria-hidden="true"
>
	<div class="modal-dialog order-transportation-modal__dialog">
		<div class="modal-content order-transportation-modal__content">
			<div class="order-transportation-modal__header">
				<div class="order-transportation-modal__title h3">
					<?= __('Заказать грузоперевозку', 'air') ?>
				</div>

				<button type="button" class="as-modal__close" data-bs-dismiss="modal" aria-label="Close">
					<svg>
						<use xlink:href="<?= get_sprite_uri() ?>#times"></use>
					</svg>
				</button>
			</div>

			<div class="modal-body order-transportation-modal__body">
				<form class="order-transportation-modal__form order-transportation-modal-form">
					<div class="order-transportation-modal-form__grid">
						<div class="order-transportation-modal-form__left">
							<div class="order-transportation-modal-form__row form-row">
								<input name="name" type="text" placeholder="<?= __('Ваше имя', 'air') ?>"
									   class="order-transportation-modal-form__form-control form-control form-validate__control"
								>

								<div class="form-error"></div>
							</div>

							<div class="order-transportation-modal-form__row form-row">
								<input name="phone" type="tel" placeholder="<?= __('Ваш номер телефона', 'air') ?>"
									   class="order-transportation-modal-form__form-control form-control form-validate__control"
								>

								<div class="form-error"></div>
							</div>

							<div class="order-transportation-modal-form__row form-row">
								<input name="from" type="text" placeholder="<?= __('Откуда вывозить груз', 'air') ?>"
									   class="order-transportation-modal-form__form-control form-control form-validate__control"
								>

								<div class="form-error"></div>
							</div>

							<div class="order-transportation-modal-form__row form-row">
								<input name="where" type="text" placeholder="<?= __('Куда отправлять груз', 'air') ?>"
									   class="order-transportation-modal-form__form-control form-control form-validate__control"
								>

								<div class="form-error"></div>
							</div>
						</div>

						<div class="order-transportation-modal-form__right">
							<div class="order-transportation-modal-form__row form-row">
								<input name="weight" type="text" placeholder="<?= __('Вес груза', 'air') ?>"
									   class="order-transportation-modal-form__form-control form-control form-validate__control"
								>

								<div class="form-error"></div>
							</div>

							<div
								class="order-transportation-modal-form__row order-transportation-modal-form__row--textarea form-row"
							>
								<textarea name="commentary" placeholder="<?= __('Ваш комментарий', 'air') ?>"
										  class="order-transportation-modal-form__form-control form-control form-validate__control"
								></textarea>

								<div class="form-error"></div>
							</div>
						</div>
					</div>

					<div class="order-transportation-modal-form__row form-row">
						<div class="form-checkbox">
							<label class="form-checkbox__label">
								<input type="checkbox" name="privacy_policy"
									   class="form-checkbox__input form-validate__control visually-hidden"
								>

								<span class="form-checkbox__text">
									<?= __('Согласен с обработкой персональных данных', 'air') ?>
								</span>
							</label>
						</div>

						<div class="form-error"></div>
					</div>

					<?php wp_nonce_field('order-transportation-modal_form', 'order-transportation-modal_form_nonce') ?>
					<input type="hidden" name="action" value="contact_form">
					<input type="hidden" name="form" value="order-transportation-modal">

					<button type="submit" class="button button--primary order-transportation-modal-form__submit">
						<?= __('Заказать перевозку', 'air') ?>
					</button>
				</form>
			</div>
		</div>
	</div>
</div>