<?php
$header = get_field('header', 'option');
$contacts = get_field('contacts', 'option');
?>

<div class="offcanvas offcanvas-end offcanvas-menu" tabindex="-1" id="offcanvasMenu">
	<div class="offcanvas-header offcanvas-menu__header">
		<div class="offcanvas-menu__logo">
			<?= wp_get_attachment_image($header['logo'], 'medium', false, ['loading' => false]) ?>
		</div>

		<?php get_template_part('template-parts/language-switcher-offcanvas') ?>

		<button type="button" class="offcanvas-menu__close" data-bs-dismiss="offcanvas">
			<svg>
				<use xlink:href="<?= get_sprite_uri() ?>#times"></use>
			</svg>
		</button>
	</div>

	<div class="offcanvas-body offcanvas-menu__body">
		<div class="offcanvas-menu__navigation offcanvas-menu-navigation">
			<?php
			wp_nav_menu([
				'theme_location' => 'mobile_menu',
				'container' => false,
				'menu_class' => 'offcanvas-menu-navigation__list',
				'depth' => 1
			]);
			?>
		</div>

		<a href="tel:<?= sanitize_phone($contacts['phone']) ?>" class="offcanvas-menu__phone">
			<span class="offcanvas-menu__phone-icon">
				<svg>
					<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
				</svg>
			</span>
			<?= $contacts['phone'] ?>
		</a>

		<button type="button" class="button button--primary button--full offcanvas-menu__callback"
				data-bs-toggle="modal"
				data-bs-target="#modalCallback"
		>
			<span class="button__icon button__icon--left">
				<svg>
					<use xlink:href="<?= get_sprite_uri() ?>#phone"></use>
				</svg>
			</span>
			Заказать звонок
		</button>
	</div>
</div>